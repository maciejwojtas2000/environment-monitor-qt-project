#include<Arduino.h>
#include<stdio.h>
#include<string.h>
#include "dht.h"
#include <hp_BH1750.h>

dht DHT22;
hp_BH1750 BH1750;


#define DHT22PIN 2
const int pinAdc = A0;
void setup() {

  Serial.begin(9600);
  bool avail=BH1750.begin(BH1750_TO_GROUND);
}
void loop() {
  
 int chk=DHT22.read(DHT22PIN);
 
 switch(chk)
 {
  case DHTLIB_OK:
 
  break;

  
    case DHTLIB_ERROR_CHECKSUM: 
    Serial.println("Błąd sumy kontrolnej"); 
    break;

    
    case DHTLIB_ERROR_TIMEOUT: 
    Serial.println("Koniec czasu oczekiwania - brak odpowiedzi"); 
    break;

    
    default: 
    Serial.println("Nieznany błąd"); 
    break;
 }
 

 //////////WILGOTNOŚĆ I TEMPERATURA//////////////
         //wyświetlenie wartości wilgotności
  
int temp=(int)DHT22.temperature;
int wilg=(int)DHT22.humidity;
 
/////////////////////////////////////////////////
/////////////DŹWIĘK//////////////////////////////

long sum=0;
for(int i=0;i<32;i++)
{
  sum+=analogRead(pinAdc);
}
sum>>=5;



////////////////////////////////////////////////////

////////////ŚWIATŁO///////////////////////
BH1750.start();
float lux1=BH1750.getLux();
int lux=(int)lux1;
/////////////////////////////////////////

char buff[256];
sprintf(buff,"%d %d %d %d \n",temp,wilg,sum,lux);
Serial.print(buff);


}
