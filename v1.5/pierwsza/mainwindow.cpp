#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSerialPort>
#include <QSerialPortInfo>
#include <string>
#include <QDebug>
#include <QMessageBox>
#include <QPixmap>



QSerialPort *serial;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    serial=new QSerialPort(this);
    serial->setPortName("/dev/ttyACM0");
    serial->setBaudRate(QSerialPort::Baud9600);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    if(serial->open(QIODevice::ReadWrite)>0)
    {
        printf("udalo sie");
        connect(serial,SIGNAL(readyRead()),this,SLOT(serialReceived()));
    }
    else{
        printf("Nie udało się połączyć");
    }
    MainWindow::makePlot();

}

MainWindow::~MainWindow()
{
    delete ui;
    serial->close();
}

int a,b,c;
int wysw1;
int wysw2;
int wysw;
int wysw3;
float d;
int indexwysw=0;
int i=0;

void MainWindow::serialReceived()
{
    QPixmap opady0("/home/mwojtas/Pulpit/WDS/qt/pierwsza/images/opady0.png");
    QPixmap opady1("/home/mwojtas/Pulpit/WDS/qt/pierwsza/images/opady1.png");
    QPixmap opady2("/home/mwojtas/Pulpit/WDS/qt/pierwsza/images/opady2.png");
    QPixmap slonce0("/home/mwojtas/Pulpit/WDS/qt/pierwsza/images/slonce3.png");
    QPixmap slonce1("/home/mwojtas/Pulpit/WDS/qt/pierwsza/images/slonce0.png");
    QPixmap slonce2("/home/mwojtas/Pulpit/WDS/qt/pierwsza/images/slonce1.png");
    QPixmap slonce3("/home/mwojtas/Pulpit/WDS/qt/pierwsza/images/slonce2.png");
    QPixmap sam1("/home/mwojtas/Pulpit/WDS/qt/pierwsza/images/sam1.png");
    QPixmap sam2("/home/mwojtas/Pulpit/WDS/qt/pierwsza/images/sam2.png");
    QPixmap sam4("/home/mwojtas/Pulpit/WDS/qt/pierwsza/images/sam4.png");
    QByteArray ba;
    if(serial->canReadLine()>0){
        ba=serial->readAll();
       // qDebug()<<ba;
        QString Data=QString(ba);
    QTextStream myteststream(&Data);
    myteststream>>a>>b>>c>>d;

    QString s1 = QString::number(a);
    QString s2 = QString::number(b);
    QString s3 = QString::number(c);
    QString s4 = QString::number(d);
    ui->lineEdit_1->setText(s1);
    ui->lineEdit_2->setText(s2);
    ui->lineEdit_3->setText(s3);

    wysw=i;
    wysw1=a;
    wysw2=b;
    wysw3=c;
        i++;
        qDebug()<<i;
        if(b<50){
          ui->obrazek2->setPixmap(opady0);
          }
          if(b>50&&b<70)
          {
          ui->obrazek2->setPixmap(opady1);
          }
          if(b>=70)
          {
          ui->obrazek2->setPixmap(opady2);
          }

          if(a<15){
          ui->obrazek1->setPixmap(slonce0);
          }
          if(a>15&&a<25)
          {
          ui->obrazek1->setPixmap(slonce1);
          }
          if(a>25&&a<=30)
          {
          ui->obrazek1->setPixmap(slonce2);
          }
          if(a>30){
          ui->obrazek1->setPixmap(slonce3);
          }


          if(c<150)
          {
          ui->obrazek3->setPixmap(sam1);
          }
          if(c>150&&c<300)
        {
          ui->obrazek3->setPixmap(sam2);
          }
          if(c>300)
          {
          ui->obrazek3->setPixmap(sam4);
          }



        ui->customPlot->addGraph(); // give the axes some labels:
        ui->customPlot->xAxis->setLabel("");
        ui->customPlot->yAxis->setLabel("Temp");
        // set axes ranges, so we see all data:
        ui->customPlot->xAxis->setRange(i-100, i);
        ui->customPlot->yAxis->setRange(0, 50);
        ui->customPlot->graph(0)->addData(wysw, wysw1);
        ui->customPlot->replot();



        ui->customPlot1->addGraph(); // give the axes some labels:
        ui->customPlot1->xAxis->setLabel("");
        ui->customPlot1->yAxis->setLabel("Wilg.");
        // set axes ranges, so we see all data:
        ui->customPlot1->xAxis->setRange(i-100, i+20);
        ui->customPlot1->yAxis->setRange(0, 100);
        ui->customPlot1->graph(0)->addData(wysw, wysw2);
        ui->customPlot1->replot();

        ui->customPlot2->addGraph(); // give the axes some labels:
        ui->customPlot2->xAxis->setLabel("");
        ui->customPlot2->yAxis->setLabel("Hałas");
        // set axes ranges, so we see all data:
        ui->customPlot2->xAxis->setRange(i-100, i+20);
        ui->customPlot2->yAxis->setRange(0, 500);
        ui->customPlot2->graph(0)->addData(wysw, wysw3);
        ui->customPlot2->replot();


        }
}


void MainWindow::makePlot(){}
    // generate some data:
  /*  QVector<double> x(100), y(100); // initialize with entries 0..100
     x[0]=0;
     y[0]=a;
     x[1]=1;
     y[1]=a;
     x[2]=2;
     y[2]=a;
     x[3]=3;
     y[3]=a;
     x[4]=4;
     y[4]=a;
     x[5]=5;
     y[5]=a;
     // create graph and assign data to it:
      ui->customPlot->addGraph();
      ui->customPlot->graph(0)->setData(x, y);
      // give the axes some labels:
      ui->customPlot->xAxis->setLabel("x");
      ui->customPlot->yAxis->setLabel("y");
      // set axes ranges, so we see all data:
      ui->customPlot->xAxis->setRange(0, 10);
      ui->customPlot->yAxis->setRange(0, 30);
      ui->customPlot->replot();

}
*/
