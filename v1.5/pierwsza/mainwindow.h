#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

extern int a,b,c,indexwysw;
extern int wysw;
extern int wysw1;
extern int wysw2;
extern int wysw3;
extern float d;
namespace Ui {
class MainWindow;
}
/*! \class MainWindow


  Klasa odpowiedzialna za główne okno programu.

*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    /*! \fn serialReceived()

        Metoda odpowiedzialna za pobieranie danych, wyświetlanie ich w oknie głównym, rysowanie grafów oraz wyświetlanie obrazków.

    */
    /*!

Metoda wykorzystuje QCutomPlot(rysowanie grafów), Pixmap(wyświetlanie obrazków)
    */
    void serialReceived();
    void makePlot();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
